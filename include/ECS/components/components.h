#pragma once

#include <bitset>
#include <cstdint>
#include <glm/vec2.hpp>

#include "ECS/core/definitions.h"

namespace krum {

static inline ComponentType GetComponentType() noexcept {
  static ComponentType last_id = 0;
  return last_id++;
}

template <typename T>
static inline ComponentType GetComponentType() noexcept {
  static ComponentType component_id = GetComponentType();
  return component_id;
}

using Signature = std::bitset<kMaxComponents>;

struct Transform {
  glm::vec2 position;
  glm::vec2 rotation;
  glm::vec2 scale;
};

struct Velocity {
  glm::vec2 velocity;
};

struct Animation {};

struct KeyboardController {};

}  // namespace krum