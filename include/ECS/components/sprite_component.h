#pragma once

#include <SDL2/SDL.h>

#include <memory>

#include "assets/asset_texture.h"

namespace krum {

struct Sprite {
  // std::shared_ptr<SDL_Texture> tex_sptr;
  // SDL_Rect src_rect
  std::shared_ptr<krum::TextureAsset> asset;
};

}  // namespace krum
