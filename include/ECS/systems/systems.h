#pragma once

#include "ECS/systems/base_system.h"
#include "ECS/systems/physics_system.h"
#include "ECS/systems/render_system.h"