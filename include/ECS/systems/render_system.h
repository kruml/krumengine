#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <memory>

#include "ECS/components/components.h"
#include "ECS/components/sprite_component.h"
#include "ECS/core/coordinator.h"
#include "ECS/systems/base_system.h"

namespace krum {
class RenderSystem : public System {
 public:
  // TODO: Give coordinator as argument instead
  RenderSystem()
      : coordinator_(Coordinator::GetInstance()), ren_ptr_(nullptr) {}

  ~RenderSystem() {
    if (ren_ptr_ != NULL) {
      SDL_DestroyRenderer(ren_ptr_);
    }
  }

  void SetRenderer(SDL_Renderer* ren_ptr) { ren_ptr_ = ren_ptr; }

  void inline Update(float dt) {
    SDL_RenderClear(ren_ptr_);
    for (auto const& entity : entities_) {
      auto& transform = coordinator_.GetComponent<Transform>(entity);
      auto& sprite = coordinator_.GetComponent<Sprite>(entity);

      sprite.asset->dest.x = transform.position.x;
      sprite.asset->dest.y = transform.position.y;
      sprite.asset->dest.h = sprite.asset->src.h * transform.scale.x;
      sprite.asset->dest.w = sprite.asset->src.w * transform.scale.y;

      SDL_RenderCopyEx(ren_ptr_, sprite.asset->tex_ptr, &sprite.asset->src,
                       &sprite.asset->dest, 0, NULL, SDL_FLIP_NONE);
    }

    SDL_RenderPresent(ren_ptr_);
  }

 private:
  Coordinator& coordinator_;
  SDL_Renderer* ren_ptr_;
};
}  // namespace krum
