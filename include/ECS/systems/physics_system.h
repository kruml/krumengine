#pragma once

#include "ECS/core/coordinator.h"
#include "ECS/core/definitions.h"
#include "ECS/systems/base_system.h"

namespace krum {

class PhysicsSystem : public System {
 public:
  PhysicsSystem() : coordinator_(Coordinator::GetInstance()) {}
  void inline Update(float dt) {
    // auto& instance_coord = krum::Coordinator::GetInstance();

    for (auto const& entity : entities_) {
      auto& rigid_body = coordinator_.GetComponent<Velocity>(entity);
      auto& transform = coordinator_.GetComponent<Transform>(entity);

      transform.position += rigid_body.velocity * dt;
    }
  }

 private:
  Coordinator& coordinator_;
};
}  // namespace krum
