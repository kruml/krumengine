#pragma once

#include <cstdint>
#include <set>

#include "ECS/components/components.h"
//#include "ECS/core/coordinator.h"
#include "ECS/core/definitions.h"

class Coordinator;
namespace krum {
using SystemType = uint8_t;
const int kMaxSystems = 32;

static inline SystemType GetSystemType() noexcept {
  static SystemType last_id = 0;
  return last_id++;
}

template <typename T>
static inline SystemType GetSystemType() noexcept {
  static SystemType system_id = GetSystemType();
  return system_id;
}

class System {
 public:
  std::set<Entity> entities_;
  Signature signature_;
};

}  // namespace krum