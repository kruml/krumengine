#pragma once

#include <array>
#include <unordered_map>

#include "ECS/core/definitions.h"

namespace krum {

class IComponentArray {
 public:
  virtual ~IComponentArray() = default;
  virtual void EntityDestroyed(Entity entity) = 0;
};

template <typename T>
class ComponentArray : public IComponentArray {
 public:
  void InsertData(Entity entity, T component) {
    assert(entity_to_idx_.find(entity) == entity_to_idx_.end() &&
           "Component already added to entity!");
    component_array_[size_] = component;
    entity_to_idx_[entity] = size_;
    idx_to_entity_[size_] = entity;
    size_++;
  }

  void RemoveData(Entity entity) {
    size_t removed_idx = entity_to_idx_[entity];
    size_t last_idx = size_ - 1;

    Entity last_entity = idx_to_entity_[last_idx];
    component_array_[removed_idx] = component_array_[last_idx];
    entity_to_idx_[last_entity] = removed_idx;
    idx_to_entity_[removed_idx] = last_entity;

    entity_to_idx_.erase(entity);
    idx_to_entity_.erase(last_idx);

    size_--;
  }

  T& GetData(Entity entity) {
    assert(entity_to_idx_.find(entity) != entity_to_idx_.end() &&
           "Entity does not have component!");

    return component_array_[entity_to_idx_[entity]];
  }

  void EntityDestroyed(Entity entity) override {
    if (entity_to_idx_.find(entity) != entity_to_idx_.end()) {
      RemoveData(entity);
    }
  }

 private:
  std::array<T, kMaxEntities> component_array_;
  std::unordered_map<Entity, size_t> entity_to_idx_;
  std::unordered_map<size_t, Entity> idx_to_entity_;
  size_t size_ = 0;
};

}  // namespace krum