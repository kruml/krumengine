#pragma once

#include <bitset>
#include <cstdint>

namespace krum {
using Entity = std::uint32_t;
const Entity kMaxEntities = 5000;

using ComponentType = std::uint8_t;
const ComponentType kMaxComponents = 32;

using Signature = std::bitset<kMaxComponents>;

}  // namespace krum