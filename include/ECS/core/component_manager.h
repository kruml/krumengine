#pragma once

#include <memory>
#include <unordered_map>

#include "ECS/components/components.h"
#include "ECS/core/component_array.h"
#include "ECS/core/definitions.h"

namespace krum {

class ComponentManager {
 public:
  template <typename T>
  void AddComponent(Entity entity, T component) {
    // Create component array if needed
    ComponentType type = GetComponentType<T>();

    if (component_arrays_.find(type) == component_arrays_.end()) {
      component_arrays_.insert(
          std::make_pair(type, std::make_shared<ComponentArray<T>>()));
    }
    GetComponentArray<T>()->InsertData(entity, component);
  }

  template <typename T>
  void RemoveComponent(Entity entity) {
    auto const& type = GetComponentType<T>();
    assert(component_arrays_.find(type) != component_arrays_.end() &&
           "Component not attached to entity!");

    GetComponentArray<T>()->RemoveData(entity);
  }

  template <typename T>
  inline T& GetComponent(Entity entity) {
    auto const& type = GetComponentType<T>();
    assert(component_arrays_.find(type) != component_arrays_.end() &&
           "Component not attached to entity!");

    return GetComponentArray<T>()->GetData(entity);
  }

 private:
  std::unordered_map<ComponentType, std::shared_ptr<IComponentArray>>
      component_arrays_;

  template <typename T>
  std::shared_ptr<ComponentArray<T>> GetComponentArray() {
    assert(component_arrays_.find(GetComponentType<T>()) !=
               component_arrays_.end() &&
           "Component not added");

    return std::static_pointer_cast<ComponentArray<T>>(
        component_arrays_[GetComponentType<T>()]);
  }
};

}  // namespace krum