#pragma once

#include <assert.h>

#include <array>
#include <queue>

#include "ECS/core/definitions.h"

namespace krum {

class EntityManager {
 public:
  EntityManager() : living_entitites_(0) {
    for (Entity id = 0; id < kMaxEntities; id++) {
      available_ids_.push(id);
      signatures_[id].reset();
    }
  }

  Entity CreateEntity() {
    assert(living_entitites_ < kMaxEntities &&
           "Maximum ammount of entities already created!");

    Entity new_entity = available_ids_.front();
    available_ids_.pop();
    ++living_entitites_;

    return new_entity;
  }

  void DestroyEntity(Entity id) {
    assert(id <= kMaxEntities && "Entity ID out of range!");
    signatures_[id].reset();
    available_ids_.push(id);
    living_entitites_--;
  }

  void SetSignature(Entity id, Signature signature) {
    assert(id <= kMaxEntities && "Entity ID out of range!");
    signatures_[id] = signature;
  }

  Signature GetSignature(Entity id) { return signatures_[id]; }

 private:
  uint32_t living_entitites_{};
  std::queue<Entity> available_ids_{};
  std::array<Signature, kMaxEntities> signatures_{};
};

}  // namespace krum