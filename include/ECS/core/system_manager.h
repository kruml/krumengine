#pragma once

#include <memory>
#include <unordered_map>

#include "ECS/core/definitions.h"
#include "ECS/systems/base_system.h"

namespace krum {

class SystemManager {
 public:
  template <typename T>
  std::shared_ptr<T> RegisterSystem() {
    SystemType type = GetSystemType<T>();
    assert(systems_.find(type) == systems_.end() && "System already added!");
    auto system = std::make_shared<T>();
    systems_.insert(std::make_pair(type, system));
    return system;
  }

  template <typename T>
  void UnregisterSystem() {
    SystemType type = GetSystemType<T>();
    assert(systems_.find(type) != systems_.end() && "System not registered!");

    systems_.erase(type);
  }

  template <typename T>
  void SetSignature(Signature signature) {
    SystemType type = GetSystemType<T>();
    assert(systems_.find(type) != systems_.end() && "System not registered!");

    systems_.find(type)->second->signature_ = signature;
  }

  void EntityDestoryed(Entity entity) {
    for (auto const& pair : systems_) {
      auto const& system = pair.second;
      system->entities_.erase(entity);
    }
  }

  void EntitySignatureChanged(Entity entity, Signature entity_signature) {
    for (auto const& pair : systems_) {
      auto const& system = pair.second;

      if ((entity_signature & system->signature_) == system->signature_) {
        system->entities_.insert(entity);
      } else {
        system->entities_.erase(entity);
      }
    }
  }

 private:
  std::unordered_map<SystemType, std::shared_ptr<System>> systems_;
};

}  // namespace krum