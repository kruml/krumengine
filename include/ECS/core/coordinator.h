#pragma once

#include <memory>

#include "ECS/core/component_manager.h"
#include "ECS/core/entity_manager.h"
#include "ECS/core/system_manager.h"

namespace krum {

class Coordinator {
 public:
  /* Delete for singleton */
  Coordinator(Coordinator const&) = delete;
  void operator=(Coordinator const&) = delete;

  static inline Coordinator& GetInstance() {
    static Coordinator instance;
    return instance;
  }

  void Init() {
    component_manager_ = std::make_unique<ComponentManager>();
    entity_manager_ = std::make_unique<EntityManager>();
    system_manager_ = std::make_unique<SystemManager>();
  }

  Entity CreateEntity() { return entity_manager_->CreateEntity(); }

  void DestroyEntity(Entity entity) { entity_manager_->DestroyEntity(entity); }

  template <typename T>
  void AddComponent(Entity entity, T component) {
    component_manager_->AddComponent<T>(entity, component);
    auto signature = entity_manager_->GetSignature(entity);
    signature.set(GetComponentType<T>());
    entity_manager_->SetSignature(entity, signature);

    system_manager_->EntitySignatureChanged(entity, signature);
  }

  template <typename T>
  void RemoveComponent(Entity entity) {
    component_manager_->RemoveComponent<T>(entity);
  }

  template <typename T>
  inline T& GetComponent(Entity entity) {
    return component_manager_->GetComponent<T>(entity);
  }

  template <typename T>
  std::shared_ptr<T> RegisterSystem() {
    return system_manager_->RegisterSystem<T>();
  }

  template <typename T>
  void SetSystemSignature(Signature signature) {
    system_manager_->SetSignature<T>(signature);
  }

 private:
  Coordinator(){};
  std::unique_ptr<ComponentManager> component_manager_;
  std::unique_ptr<EntityManager> entity_manager_;
  std::shared_ptr<SystemManager> system_manager_;
};
}  // namespace krum
