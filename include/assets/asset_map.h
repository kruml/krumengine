#pragma once

#include <assert.h>

#include <memory>
#include <string>
#include <unordered_map>

#include "asset_type.h"

namespace krum {

class IAssetMap {
 public:
  virtual void AddAsset(const std::string& /* asset_name */,
                        std::shared_ptr<Asset> /* asset */);
  virtual std::shared_ptr<Asset> GetAsset(const std::string& /* asset name */);
};

template <typename T>
class AssetMap : public IAssetMap {
 public:
  std::shared_ptr<Asset> GetAsset(const std::string& asset_name) override {
    assert(asset_map_.find(asset_name) != asset_map_.end() &&
           "Asset does not exist!");
    return asset_map_[asset_name];
  }

  void AddAsset(const std::string& asset_name,
                std::shared_ptr<Asset> asset) override {
    assert(asset_map_.find(asset_name) == asset_map_.end() &&
           "Asset already exists!");
    asset_map_.insert(std::make_pair(asset_name, asset));
  }

 private:
  std::unordered_map<std::string, std::shared_ptr<Asset>> asset_map_;
};
}  // namespace krum