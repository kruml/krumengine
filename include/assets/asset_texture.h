#pragma once

#include <SDL2/SDL.h>
#include <string.h>

#include <memory>

#include "asset_type.h"

namespace krum {
enum class TextureName { kPlayer, kMonster };
const std::string to_string(TextureName e) {
  switch (e) {
    case TextureName::kPlayer:
      return "player_texture";
    case TextureName::kMonster:
      return "monster_texture";
    default:
      return "unknown_texture";
  }
}
class TextureAsset : public Asset {
 public:
  TextureAsset(SDL_Texture* asset_ptr, SDL_Rect src_rect, SDL_Rect dest_rect)
      : tex_ptr(asset_ptr), src(src_rect), dest(dest_rect) {}
  ~TextureAsset() { SDL_DestroyTexture(tex_ptr); }
  SDL_Texture* tex_ptr;
  SDL_Rect src, dest;
};
}  // namespace krum