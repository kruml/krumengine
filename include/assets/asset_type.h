#pragma once

#include <cstdint>

namespace krum {
using AssetType = uint8_t;

static inline AssetType GetAssetType() noexcept {
  static AssetType last_id = 0;
  return last_id++;
}

template <typename T>
static inline AssetType GetAssetType() noexcept {
  static AssetType asset_id = GetAssetType();
  return asset_id;
}

class Asset {};
}  // namespace krum