#pragma once

#include <SDL2/SDL.h>
#include <assert.h>

#include <fstream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <unordered_map>

#include "assets/asset_map.h"
#include "assets/asset_type.h"

namespace krum {

/* Forward declatations */
class IAssetMap;

template <typename T>
class AssetMap;

class AssetManager {
 public:
  inline static AssetManager& GetInstance() {
    static AssetManager instance;
    return instance;
  }

  template <typename T>
  void AddAsset(std::string asset_name, std::shared_ptr<T> asset) {
    AssetType id = GetAssetType<T>();
    if (assets_.find(id) == assets_.end()) {
      assets_.insert(std::make_pair(id, AssetMap<T>()));
    }
    GetAssetMap<T>().AddAsset(asset_name, asset);
  }

  template <typename T>
  std::shared_ptr<Asset> GetAsset(const std::string& asset_name) {
    AssetType id = GetAssetType<T>();
    assert(assets_.find(id) != assets_.end() && "Asset does not exist!");
    return assets_[id].GetAsset(asset_name);
  }

 private:
  AssetManager() {}

  template <typename T>
  IAssetMap GetAssetMap() {
    return assets_[GetAssetType<T>()];
  }

  std::unordered_map<AssetType, IAssetMap> assets_;

 public:
  AssetManager(AssetManager const&) = delete;
  void operator=(AssetManager const&) = delete;
};

template <typename T>
static void LoadAssetStructure(AssetManager& asset_manager, SDL_Renderer* ren,
                               const std::string& asset_json_path) {
  nlohmann::json hej;
  std::ifstream file_stream(asset_json_path);
  file_stream >> hej;
  for (auto it : hej) {
    std::cout << "value: " << it << it["path"] << std::endl;
    std::string asset_path(asset_json_path);
    asset_path += "/";
    asset_path += it["path"];
    SDL_Surface* tmp_surf = IMG_Load(asset_path.c_str());
    if (tmp_surf == NULL) {
      std::cout << IMG_GetError() << std::endl;
      // TODO: Better exception
      assert(false);
    }

    std::shared_ptr<krum::TextureAsset> tex =
        std::make_shared<krum::TextureAsset>(
            SDL_CreateTextureFromSurface(ren, tmp_surf),
            SDL_Rect{0, 0, tmp_surf->w, tmp_surf->h},
            SDL_Rect{0, 0, tmp_surf->w, tmp_surf->h});
    asset_manager.AddAsset<TextureAsset>(to_string(TextureName::kPlayer), tex);
  }
  return;
}

}  // namespace krum