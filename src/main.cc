#include <SDL2/SDL.h>

#include <chrono>
#include <iostream>

#include "ECS/components/components.h"
#include "ECS/core/coordinator.h"
#include "ECS/core/definitions.h"
#include "ECS/systems/systems.h"
#include "assets/asset_manager.h"
#include "assets/asset_texture.h"
#include "assets/asset_type.h"

std::shared_ptr<krum::PhysicsSystem> phys_sy = nullptr;
std::shared_ptr<krum::PhysicsSystem> o = nullptr;
std::shared_ptr<krum::RenderSystem> rend_sys = nullptr;
SDL_Renderer* ren_ptr = NULL;
SDL_Window* win_ptr = NULL;

bool InitSDL(bool fullscreen = false) {
  int flags = 0;

  if (fullscreen) {
    flags = SDL_WINDOW_FULLSCREEN;
  }

  if (SDL_Init(SDL_INIT_EVERYTHING | flags) != 0) {
    // LOG_ENGINE_ERROR("SDL Subsystems failed to initialize. Error: {0}",
    // SDL_GetError());
    return false;
  }

  win_ptr = SDL_CreateWindow("Testing", SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED, 640, 480, 0);

  if (win_ptr == NULL) {
    std::cout << SDL_GetError() << std::endl;
    return false;
  }

  ren_ptr = SDL_CreateRenderer(win_ptr, -1, 0);

  if (ren_ptr == NULL) {
    std::cout << SDL_GetError() << std::endl;
    return false;
  }

  int img_init_flags = IMG_INIT_PNG;
  if ((IMG_Init(img_init_flags) & img_init_flags) != img_init_flags) {
    std::cout << SDL_GetError() << std::endl;
    return false;
  }

  SDL_SetRenderDrawColor(ren_ptr, 255, 255, 255, 255);
  SDL_RenderClear(ren_ptr);
  SDL_RenderPresent(ren_ptr);

  return true;
}

bool InitSystems() {
  auto& coordinator = krum::Coordinator::GetInstance();

  /* Add systems */
  phys_sys = coordinator.RegisterSystem<krum::PhysicsSystem>();

  krum::Signature physics_signature;
  physics_signature.set(krum::GetComponentType<krum::Transform>());
  physics_signature.set(krum::GetComponentType<krum::Velocity>());
  coordinator.SetSystemSignature<krum::PhysicsSystem>(physics_signature);

  /* Render system */
  rend_sys = coordinator.RegisterSystem<krum::RenderSystem>();
  rend_sys->SetRenderer(ren_ptr);

  krum::Signature render_signature;
  render_signature.set(krum::GetComponentType<krum::Transform>());
  render_signature.set(krum::GetComponentType<krum::Sprite>());
  coordinator.SetSystemSignature<krum::RenderSystem>(render_signature);

  // TODO: return false if error
  return true;
}

bool InitEntities() {
  auto& coordinator = krum::Coordinator::GetInstance();

  /* Add Entities */
  krum::Entity test = coordinator.CreateEntity();
  krum::Entity test2 = coordinator.CreateEntity();
  krum::Entity test3 = coordinator.CreateEntity();
  // krum::Entity sprite1 = coordinator.CreateEntity();

  krum::Transform trans{glm::vec2(0.0f, 0.0f), glm::vec2(0.0f, 0.0f),
                        glm::vec2(1.0f, 1.0f)};
  krum::Velocity rb{glm::vec2(0.5f, 0.5f)};

  /* Add Components */
  coordinator.AddComponent<krum::Transform>(test, trans);
  coordinator.AddComponent<krum::Velocity>(test, rb);
  coordinator.AddComponent<krum::Velocity>(test2, rb);
  coordinator.AddComponent<krum::Velocity>(test3, rb);
  coordinator.RemoveComponent<krum::Velocity>(test2);

  // coordinator.AddComponent<krum::Transform>(sprite1, trans);

  // TMP: Add asset and create entity with sprite
  /*
  SDL_Surface* tmp_surf = IMG_Load("../assets/textures/player_sheet.png");
  if (tmp_surf == NULL) {
    std::cout << IMG_GetError() << std::endl;
    return false;
  }

  std::shared_ptr<krum::TextureAsset> tex =
      std::make_shared<krum::TextureAsset>(
          SDL_CreateTextureFromSurface(ren_ptr, tmp_surf),
          SDL_Rect{0, 0, tmp_surf->w, tmp_surf->h},
          SDL_Rect{0, 0, tmp_surf->w, tmp_surf->h});

  krum::AssetManager& am_instance = krum::AssetManager::GetInstance();
  am_instance.AddAsset<krum::TextureAsset>(
      krum::to_string(krum::TextureName::kPlayer), tex);

  auto tmp = am_instance.GetAsset<krum::TextureAsset>(
      krum::to_string(krum::TextureName::kPlayer));

  krum::Entity tex_test = coordinator.CreateEntity();
  coordinator.AddComponent<krum::Transform>(tex_test, trans);
  */

  return true;
}

bool InitAssets() {
#ifndef ASSETS_FOLDER_PATH
  assert(false);
#endif
  std::string path(ASSETS_FOLDER_PATH);
  path += "/textures";
  std::cout << path << std::endl;

  krum::LoadAssetStructure<krum::TextureAsset>(
      krum::AssetManager::GetInstance(), ren_ptr, path);
  return true;
}

bool HandleEvents() {
  SDL_Event e;
  // Handle events on queue
  while (SDL_PollEvent(&e) != 0) {
    // User requests quit
    switch (e.type) {
      case SDL_WINDOWEVENT:
        switch (e.window.event) {
          case SDL_WINDOWEVENT_CLOSE:
            return false;
        }
    }
  }
  return true;
}

int main() {
  auto& coordinator = krum::Coordinator::GetInstance();
  coordinator.Init();

  InitSDL();
  InitSystems();
  InitAssets();
  InitEntities();

  bool run = true;

  float dt = 0.0;

  /* Game loop */
  while (run) {
    auto startTime = std::chrono::high_resolution_clock::now();
    run = HandleEvents();
    phys_sys->Update(dt);
    rend_sys->Update(dt);
    /*
    std::cout << coordinator.GetComponent<krum::Transform>(0).position.x <<
    ","
              << coordinator.GetComponent<krum::Transform>(0).position.y
              << std::endl;
              */
    auto stopTime = std::chrono::high_resolution_clock::now();
    dt = std::chrono::duration<float, std::chrono::seconds::period>(stopTime -
                                                                    startTime)
             .count();
  }

  if (ren_ptr != NULL) {
    SDL_DestroyRenderer(ren_ptr);
  }

  if (win_ptr != NULL) {
    SDL_DestroyWindow(win_ptr);
  }

  SDL_Quit();
}
