#include "assets/asset_map.h"

namespace krum {
std::shared_ptr<Asset> IAssetMap::IGetAsset(
    const std::string& /* asset_name */) {}

void IAssetMap::AddAsset(const std::string& /* asset_name */,
                         std::shared_ptr<Asset> /* asset */) {}

}  // namespace krum
